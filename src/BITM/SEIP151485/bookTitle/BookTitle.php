<?php

namespace App\BookTitle;
use App\Message\Message;
use App\Model\Database as DB;
class BookTitle extends DB
{
    public $id = "";
    public $book_title = " ";
    public $author_name = "";

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('book_title', $data)) {
            $this->book_title = $data['book_title'];
        }
        if (array_key_exists('author_name', $data)) {
            $this->author_name = $data['author_name'];
        }
    }

    /*NOT SO GOOD METHOD BECAUSE HACKERS CAN DISTURB
     public function store()
     {

         $sql = "INSERT INTO book_title(book_title,author_name)VALUES('$this->book_title','$this->author_name')";

         $STH = $this->DBH->prepare($sql);
         $STH->execute();


     }//end of store method
    */
    public function store()
    {
        $arrData = array($this->book_title, $this->author_name);
        $sql = "INSERT INTO book_title(book_title,author_name)VALUES(?,?)";
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        if ($result) {
            Message::setMessage("Success !Data has been inserted Successfully!");
        }
        else{
            Message::setMessage("Failed!Data has been inserted Successfully!");


        }
        Utility::redirect('create.php');

        /* public function index()
         {
             echo $this->book_title;
         }*/
    }
}